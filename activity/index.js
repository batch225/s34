// Activity

const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// HOME PAGE

app.get("/home", (req, res) => {
    res.json("Welcome to the home page");
});

let users = [
    {
        "username" : "ron077",
        "firstname" : "Ronald",
        "lastname" : "Bautista"
    },
    {
        "username" : "ron078",
        "firstname" : "Ron",
        "lastname" : "Bautista"
    },
]; 

// USERS PAGE

app.get("/users", (req, res) => {
    res.json(users);
});

// DELETE USERS

app.delete("/delete-user", (req, res) => {
    let message;

    let userToDelete;

	for (let i = 0; i < users.length; i++){
		
		if (req.body.username == users[i].username){
            userToDelete = i;
			message = `User ${req.body.username} has been deleted.`;
			break;
		} else {
			message = "User does not exist.";
		}
	}

    if(userToDelete !== undefined) {
        users.splice(userToDelete, 1);
    }
	res.send(message);
});

app.listen(port, () => console.log(`Server running at port ${port}`))



